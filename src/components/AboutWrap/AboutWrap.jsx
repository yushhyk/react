import React from 'react';
import AboutInfo from './AboutInfo/AboutInfo';
import AboutImage from './AboutImage/AboutImage';
import './AboutWrap.css'

export default function ProductWrap() {
    return (
        <div className='about-info-block'>
            <div className='container'>
                <div className="about-info-wrap">
                    <AboutInfo/>
                    <AboutImage/>
                </div>
            </div>
        </div>
    )
}