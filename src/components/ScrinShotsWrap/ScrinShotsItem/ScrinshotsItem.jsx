import React from 'react';
import './ScrinshotsItem.css';

export default function ScrinshotsItem() {
    return (
        <div className='scrinshots-item-block'>
            <div className='scrinshots-image'/>
            <div className="scrinshots-info-block">
                <h2 className="scrinshots__title">
                    The description for the image
                </h2>
                <div className="scrinshots-text">
                    Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem ullam
                    quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde perferendis nemo
                    debitis dolor.
                </div>
            </div>
        </div>
    )
}