import React from 'react';
import ScrinshotsTitle from './ScrinShotsTitle/ScrinshotsTitle';
import ScrinshotsItem from './ScrinShotsItem/ScrinshotsItem';
import './ScrinshotsWrap.css'

export default function ScrinshotsWrap() {
    return (
        <div className='scrinshots-wrap-block'>
            <div className='container'>
                <div className="scrinshots-info-wrap">
                    <ScrinshotsTitle/>
                    <div className="scrinshot-block">
                        <div className="scrinshot-left-wrap">
                            <ScrinshotsItem/>
                            <ScrinshotsItem/>
                        </div>

                        <div className="scrinshot-right-wrap">
                            <ScrinshotsItem/>
                            <ScrinshotsItem/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}