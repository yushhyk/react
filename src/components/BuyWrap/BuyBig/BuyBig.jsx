import React from 'react';
import './BuyBig.css';

export default function BuyBig() {
    return (
        <div className="buy-card">
            <h3 className="buy-card-title">
                Lux
            </h3>

            <div className="buy-price">
                $200
            </div>

            <ul className="buy-list">
                <li className="buy-list-item">1. Porro officia cumque sint deleniti; </li>
                <li className="buy-list-item">2. Тemo facere rem vitae odit;</li>
                <li className="buy-list-item">3. Cum odio, iste quia doloribus autem;</li>
                <li className="buy-list-item">4. Aperiam nulla ea neque.</li>
                <li className="buy-list-item">1. Porro officia cumque sint deleniti; </li>
                <li className="buy-list-item">2. Тemo facere rem vitae odit;</li>
                <li className="buy-list-item">3. Cum odio, iste quia doloribus autem;</li>
                <li className="buy-list-item">4. Aperiam nulla ea neque.</li>
                <li className="buy-list-item">1. Porro officia cumque sint deleniti; </li>
                <li className="buy-list-item">2. Тemo facere rem vitae odit;</li>
                <li className="buy-list-item">3. Cum odio, iste quia doloribus autem;</li>
                <li className="buy-list-item">4. Aperiam nulla ea neque.</li>
            </ul>

            <button type='submit' className="buy-button">
                BUY
            </button>
        </div>
    )
}