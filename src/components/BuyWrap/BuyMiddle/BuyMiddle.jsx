import React from 'react';
import './BuyMiddle.css';

export default function BuyMiddle() {
    return (
        <div className="buy-card">
            <h3 className="buy-card-title">
                Premium
            </h3>

            <div className="buy-price">
                $150
            </div>

            <ul className="buy-list">
                <li className="buy-list-item">1. Porro officia cumque sint deleniti; </li>
                <li className="buy-list-item">2. Тemo facere rem vitae odit;</li>
                <li className="buy-list-item">3. Cum odio, iste quia doloribus autem;</li>
                <li className="buy-list-item">4. Aperiam nulla ea neque.</li>
                <li className="buy-list-item">1. Porro officia cumque sint deleniti; </li>
                <li className="buy-list-item">2. Тemo facere rem vitae odit;</li>
                <li className="buy-list-item">3. Cum odio, iste quia doloribus autem;</li>
                <li className="buy-list-item">4. Aperiam nulla ea neque.</li>
            </ul>

            <button type='submit' className="buy-button">
                BUY
            </button>
        </div>
    )
}