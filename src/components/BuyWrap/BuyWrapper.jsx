import React from 'react';
import BuyTitle from './BuyTitle/BuyTitle';
import BuySmall from './BuySmall/BuySmall';
import BuyMiddle from './BuyMiddle/BuyMiddle';
import BuyBig from './BuyBig/BuyBig';
import './BuyWrapper.css'

export default function BuyWrapper() {
    return (
        <div className='buy-info-block'>
            <div className='container'>
                <div className="buy-info-wrap">
                    <BuyTitle/>
                    <div className="buy-cards">
                        <BuySmall/>
                        <BuyMiddle/>
                        <BuyBig/>
                    </div>
                </div>
            </div>
        </div>
    )
}