import React from 'react';
import './BuyTitle.css';

export default function BuyTitle() {
    return (
        <h2 className="buy-title">Buy it now</h2>
    )
}