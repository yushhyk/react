import React from 'react';
import './ContactTitle.css';

export default function ContactTitle() {
    return (
        <h2 className="contact-title">Contacts</h2>
    )
}