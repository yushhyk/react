import React from 'react';
import './ContactForm.css';

export default function ContactForm() {
    return (
        <div className='contact-form__block'>
            <input className='contact-form__name' placeholder='Your name:' type="text"/>
            <input className='contact-form__email' placeholder='Your email:' type="email"/>
            <textarea name="" className='contact-form__message' placeholder='Your message:'/>
                <button type='submit' className='contact-form__send'>SEND</button>
        </div>
    )
}