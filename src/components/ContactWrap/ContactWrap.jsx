import React from 'react';
import ContactTitle from './ContactTitle/ContactTitle';
import ContactForm from './ContactForm/ContactForm';
import ContactSocial from './ContactSocial/ContactSocial';
import './ContactWrap.css'

export default function ContactWrap() {
    return (
        <div className='contact-info-block'>
            <div className='container'>
                <div className="contact-info-wrap">
                    <ContactTitle/>
                    <div className="contact-form-block">
                      <ContactForm/>
                      <ContactSocial/>
                    </div>

                </div>
            </div>
        </div>
    )
}