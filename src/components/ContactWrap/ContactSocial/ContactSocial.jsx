import React from 'react';
import './ContactSocial.css';

export default function ContactForm() {
    return (
        <div className='contact-social__block'>
        <ul className="contact-networks__block">
            <li className="contact-networks__item"><i className="fab fa-skype"/>here_your_login_skype</li>
            <li className="contact-networks__item"><i className="fab fa-facebook"/>here_your_login_facebook</li>
            <li className="contact-networks__item"><i className="far fa-envelope"/>psdhtmlcss@mail.ru</li>
            <li className="contact-networks__item"><i className="fas fa-phone"/>80 00 4568 55 55</li>
        </ul>
            <div className="contact-icons__block">
                <i className="fab fa-twitter-square"/>
                <i className="fab fa-facebook"/>
                <i className="fab fa-linkedin"/>
                <i className="fab fa-google-plus-square"/>
                <i className="fab fa-youtube-square"/>
            </div>
        </div>
    )
}