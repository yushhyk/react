import React from 'react';
import RewiewsTitle from './RewiewsTitle/RewiewsTitle';
import RewiewsItem from './RewiewsItem/RewiewsItem';
import './RewiewsWrap.css'

export default function RewiewsWrap() {
    return (
        <div className='rewiews-wrap-block'>
            <div className='container'>
                <div className="rewiews-info-wrap">
                    <RewiewsTitle/>
                    <div className="rewiews-block">
                        <div className="rewiews-left-wrap">
                            <RewiewsItem/>
                            <RewiewsItem/>
                        </div>

                        <div className="rewiews-right-wrap">
                            <RewiewsItem/>
                            <RewiewsItem/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}