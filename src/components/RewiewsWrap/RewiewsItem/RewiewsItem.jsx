import React from 'react';
import './RewiewsItem.css';

export default function RewiewsItem() {
    return (
        <div className='rewiews-item-block'>
            <div className='rewiews-image'/>

            <div className="rewiews-info-block">

                <div className="rewiews-text">
                    Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste quia
                    doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus officiis sapiente
                    necessitatibus commodi consectetur?
                </div>
                <div className="rewiews-subtitle">
                    Lourens S.
                </div>
            </div>
        </div>
    )
}