import React from 'react';
import './DignityItem.css';

export default function DignityItem() {
    return (
        <div className='dignity-item-block'>
            <div className='dignity-icon'>
                <i className="fas fa-plus dignity-icon-plus"/>
            </div>
            <p className='dignity-item-text'>Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque,
                impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime,
                quaerat porro totam, dolore minus inventore.</p>
        </div>
    )
}