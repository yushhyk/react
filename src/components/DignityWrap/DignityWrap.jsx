import React from 'react';
import DignityTitle from './DignityTitle/DignityTitle';
import DignityItem from './DignityItem/DignityItem';
import './DignityWrap.css'

export default function DignityWrap() {
    return (
        <div className='dignity-info-block'>
            <div className='container'>
                <div className="dignity-info-wrap">
                    <DignityTitle/>
                    <div className="dignity-plus-wrap">
                        <div className="dignity-left-wrap">
                            <DignityItem/>
                            <DignityItem/>
                            <DignityItem/>
                        </div>

                        <div className="dignity-right-wrap">
                            <DignityItem/>
                            <DignityItem/>
                            <DignityItem/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}