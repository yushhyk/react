import React from 'react';
import './ProductInfo.css';


export default function ProductInfo() {
    return (
        <div className='product-info__block'>
            <h1 className='product-info__title'>Product name</h1>
            <ul className="product-info__list">
                <li className="product-info-list__item"><i className="fas fa-check product-info-list__icon" />Put on this page information about your product</li>
                <li className="product-info-list__item"><i className="fas fa-check product-info-list__icon" />A detailed description of your product</li>
                <li className="product-info-list__item"><i className="fas fa-check product-info-list__icon" />Tell us about the advantages and merits</li>
                <li className="product-info-list__item"><i className="fas fa-check product-info-list__icon" />Associate the page with the payment system</li>
            </ul>
        </div>
    )
}
