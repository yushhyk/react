import React from 'react';
import ProductInfo from './ProductInfo/ProductInfo';
import ProductImage from './ProductImage/ProductImage';
import './ProductWrap.css'

export default function ProductWrap() {
    return (
        <div className='product-info-block'>
            <div className='container'>
                <div className="product-info-wrap">
                    <ProductInfo/>
                    <ProductImage/>
                </div>
            </div>
        </div>
    )
}