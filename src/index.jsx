import React from 'react';
import ReactDOM from 'react-dom';
import ProductWrap from './components/ProductWrap/ProductWrap';
import AboutWrap from './components/AboutWrap/AboutWrap'
import DignityWrap from './components/DignityWrap/DignityWrap';
import ScrinshotsWrap from './components/ScrinShotsWrap/ScrinshotsWrap';
import RewiewsWrap from './components/RewiewsWrap/RewiewsWrap'
import ContactWrap from './components/ContactWrap/ContactWrap';
import BuyWrapper from './components/BuyWrap/BuyWrapper';
import Footer from './components/Footer/Footer';
import './styles/styles.css';


const root = document.getElementById('root');

ReactDOM.render(
    <>
<ProductWrap/>
<AboutWrap/>
<DignityWrap/>
<ScrinshotsWrap/>
<RewiewsWrap/>
<BuyWrapper/>
<ContactWrap/>
<Footer/>
</>, root
);