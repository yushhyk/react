const fs = require('fs');
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');

const analyze = process.argv.includes('--analyze');
const isDev = process.env.NODE_ENV !== 'production';
const ROOT_DIR = fs.realpathSync(process.cwd());


function getName(ext) {
    return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${ext}`
}

function getImageName(ext) {
    return `[name].[${isDev ? 'hash:8' : 'contenthash'}].${ext}`
}

function pathResolve(...args) {
    return path.resolve(ROOT_DIR, ...args);
}

module.exports = {
    mode: isDev ? 'development' : 'production',
    entry: {
        main:'./src/index.jsx'
    },
    output: {
        path: pathResolve('dist'),
        filename: getName('js')
    },

    plugins: [
        analyze && new BundleAnalyzerPlugin(),
        new HTMLWebpackPlugin({
            template: "./src/index.html"
        }),

        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'css/[name].css'
        }),

        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'public'),
                to: path.resolve(__dirname, __dirname, 'dist/public')
            }
        ]),

    ].filter(Boolean),

    resolve: {
        extensions: ['.js', '.json', '.jsx', '.css']
    },

    devServer: {
        port: 3001,
        open: 'chrome'
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },

            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader','sass-loader'],
            },

            {
                test: /\.less$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader','less-loader'],
            },

            {
                test: /\.jsx$/,
                exclude: [/node-modules/],
                use: ['babel-loader']
            },

            {
                test: /\.(ttf|woff|woff2)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            outputPath: 'fonts'
                        }
                    }
                ]
            },

            {
                test: /\.(png|jpg|svg|jpeg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: `assets/${getImageName('[ext]')}`,
                        }
                    }
                ]
            },
        ]
    }
};